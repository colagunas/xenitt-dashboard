from flask_login import UserMixin
from . import db
from datetime import datetime

class Organization(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    rut = db.Column(db.String(10), unique=True)
    name = db.Column(db.String(100))
    users = db.relationship('User', backref='organization', lazy=True)

class Company(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(100), unique=True)
    db_name = db.Column(db.String(100), unique=True)
    end_contract = db.Column(db.DateTime, nullable=True, default=datetime.utcnow)
    provider_id = db.Column(db.Integer, db.ForeignKey('company.id'), nullable=True)
    clients = db.relationship('Company', remote_side=[id])
    users = db.relationship('User', backref='company', lazy=False)
    
class User(UserMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    email = db.Column(db.String(100), unique=True)
    password = db.Column(db.String(100))
    name = db.Column(db.String(1000))
    last_name = db.Column(db.String(1000))
    organization_id = db.Column(db.Integer, db.ForeignKey('organization.id'), nullable=False)
    company_id = db.Column(db.Integer, db.ForeignKey('company.id'), nullable=True)
    is_admin = db.Column(db.Boolean, default=False)
    is_active = db.Column(db.Boolean, default=False)

class Sensor(db.Model):
    client = db.Column(db.String(100), db.ForeignKey('company.db_name'), nullable=False)
    id_uuid = db.Column(db.String(10), primary_key=True)
    sensor_label = db.Column(db.String(100))
    measured_on = db.Column(db.DateTime, nullable=False, default=datetime.utcnow)
from flask import Blueprint, render_template, redirect, url_for, request, flash
from werkzeug.security import generate_password_hash, check_password_hash
from flask_login import login_required, current_user
from sqlalchemy.orm import aliased
from . import db
from .models import User, Organization, Company
from datetime import datetime
import unidecode

main = Blueprint('main', __name__)

@main.route('/')
def base():
        if current_user.is_authenticated:
                return redirect(url_for('main.panel'))
        else:
                return redirect(url_for('auth.login'))

@main.route('/panel')
@login_required
def panel():
        prov_id = current_user.company_id
        if current_user.is_admin:
                clients = Company.query.all()
        else:
                if prov_id == None:
                        clients = []
                else:
                        clients = Company.query.filter_by(provider_id=prov_id)
        clientData = []
        for c in clients:
                clientData.append(c.db_name)
        return render_template('panel.html', name=current_user.name, clientes=clientData)

@main.route('/users')
@login_required
def users():
        if current_user.is_admin:
                data = User.query.join(Organization, User.organization_id==Organization.id).outerjoin(Company, User.company_id==Company.id).add_columns(Organization.name.label("org_name"), User.id, User.email, User.name, User.last_name, Company.name.label("com_name"), User.is_active).filter(User.id != current_user.id)
                return render_template('users.html', users=data)
        else:
                return render_template('pages-404-alt.html')

@main.route('/activate', methods=['POST'])
@login_required
def activate():
        userId = request.form.get('userId')
        user = User.query.filter_by(id=userId).first()
        if user.is_active:
                user.is_active = False
        else:
                user.is_active = True
        from . import db
        db.session.commit()
        return redirect(url_for('main.users'))

@main.route('/users/edit/<int:user_id>')
@login_required
def edit_user(user_id):
        userId = user_id
        userData = User.query.filter_by(id=userId).first()
        comData = Company.query.all()
        return render_template('edit-user.html', user=userData, com=comData)

@main.route('/users/edit/<int:user_id>', methods=['POST'])
@login_required
def edit_user_post(user_id):
        userId = user_id
        userData = User.query.filter_by(id=userId).first()
        userData.email = request.form.get("email")
        userData.name = request.form.get("name")
        userData.last_name = request.form.get("last_name")
        if request.form.get("company") == "-1":
                userData.company_id = None
        else:
                userData.company_id = request.form.get("company")
        db.session.commit()
        return redirect(url_for('main.users'))

@main.route('/companies')
@login_required
def companies():
        comAlias = aliased(Company)
        comData = Company.query.outerjoin(comAlias, Company.clients).add_columns(Company.db_name, Company.id, Company.end_contract, Company.name, comAlias.name.label("provider"))
        return render_template('companies.html', companies=comData)

@main.route('/companies/edit/<int:com_id>')
@login_required
def edit(com_id):
        comId = com_id
        comData = Company.query.filter_by(id=comId).first()
        provData = Company.query.filter(Company.id!=comId)
        return render_template('edit.html', data=comData, prov=provData)

@main.route('/companies/edit/<int:com_id>', methods=['POST'])
@login_required
def edit_post(com_id):
        comId = com_id
        company = Company.query.filter_by(id=comId).first()
        company.name = request.form.get("company-name")
        provdate = request.form.get("end-date")
        date = datetime.strptime(provdate, '%Y-%m-%d %H:%M')
        company.end_contract = date
        if request.form.get("provider") == "-1":
                company.provider_id = None
        else:
                company.provider_id = request.form.get("provider")
        db.session.commit()
        return redirect(url_for('main.companies'))

@main.route('/companies/register')
@login_required
def register():
        provData = Company.query.all()
        return render_template('register.html', prov=provData)

@main.route('/companies/register', methods=['POST'])
@login_required
def register_post():
        name = request.form.get('company-name')
        provdate = request.form.get('end-date')
        date = datetime.strptime(provdate, '%Y-%m-%d %H:%M')
        udecode_name = unidecode.unidecode(name)
        dbname = udecode_name.lower().strip().replace(" ", "_")
        if request.form.get("provider") == "-1":
                provider = None
        else:
                provider = request.form.get("provider")

        comp = Company.query.filter_by(name=name).first()

        if comp:
                flash('Ya se registró una empresa con ese nombre')
                return redirect(url_for('main.register'))
        else:
                if date and name:
                        new_company = Company(name=name, db_name=dbname, end_contract=date, provider_id=provider)
                        db.session.add(new_company)
                        db.session.commit()
                        return redirect(url_for('main.companies'))
                else:
                       flash('No fueron completados todos los campos.')
                       return redirect(url_for('main.register')) 

@main.route('/companies/delete/<int:com_id>')
@login_required
def delete_company(com_id):
        comId = com_id
        company = Company.query.filter_by(id=comId).first()
        if company:
                clients = Company.query.filter_by(provider_id=comId).first()
                if clients != None:
                        flash('Esta empresa no puede ser eliminada ya que tiene clientes.')
                        return redirect(url_for('main.companies'))   
                else:
                        db.session.delete(company)
                        db.session.commit()
        return redirect(url_for('main.companies'))

@main.route('/users/register')
@login_required
def user_signup():
        orgdata = Organization.query.all()
        return render_template('user_signup.html', organizations=orgdata)

@main.route('/users/register', methods=['POST'])
def user_post():
    email = request.form.get('email')
    name = request.form.get('name')
    last_name = request.form.get('lastname')
    password = request.form.get('password')
    organization_id = request.form.get('organization')

    user = User.query.filter_by(email=email).first()

    if user:
        flash('El correo electrónico ya se encuentra registrado.')
        return redirect(url_for('main.user_signup'))
    else:
        new_user = User(email=email, password=generate_password_hash(password, method='sha256'), name=name, last_name=last_name, organization_id=organization_id)
        db.session.add(new_user)
        db.session.commit()
    
    return redirect(url_for('main.users'))